// Saber_ConsoleApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <sstream>
#include "ListRand.h"

int main()
{
	//Initialize ListRand
	auto Original = new ListRand;
	Original->CreateNodes(5);
	for (int i = 0; i < 5; ++i)
		(*Original)[i]->Data = std::to_string(i);

	(*Original)[1]->Data = "one";
	(*Original)[2]->Data = "Two Second";
	(*Original)[3]->Data = "3_";
	(*Original)[4]->Data = "4[}{0TT@B\\)4]";

	(*Original)[0]->Rand = (*Original)[0];
	(*Original)[1]->Rand = (*Original)[3];
	(*Original)[2]->Rand = (*Original)[0];
	(*Original)[4]->Rand = (*Original)[0];

	//Empty lists for deserialization
	auto CopySimple = new ListRand;
	auto CopyJSON = new ListRand;

	//IOstreams for serializtion/sederialization
	std::stringstream streamSimple;
	std::stringstream streamJSON;

	
	//Simple serialization in line
	std::cout << "Simple Serialization:" << std::endl;
	Original->Serialize(streamSimple);
	std::cout << streamSimple.str() << std::endl << std::endl;

	//Simple deserialization from Serialization result
	std::cout << "Simple Deserialization:" << std::endl;
	CopySimple->Deserialize(streamSimple);
	streamSimple.str("");
	CopySimple->Serialize(streamSimple);
	std::cout << streamSimple.str() << std::endl << std::endl;

	//Serialization in JSON
	std::cout << "Serialization in JSON:" << std::endl;
	Original->SerializeJSON(streamJSON);
	std::cout << streamJSON.str() << std::endl << std::endl;

	//Deserialization from JSON
	std::cout << "JSON Deserialization:" << std::endl;
	CopyJSON->DeserializeJSON(streamJSON);
	streamJSON.str("");
	CopyJSON->SerializeJSON(streamJSON);
	std::cout << streamJSON.str() << std::endl << std::endl;
	
    return 0;
}
