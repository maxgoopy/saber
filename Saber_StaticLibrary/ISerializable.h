#pragma once
#include<fstream>

namespace UtilsJSON 
{
	class Object;
}

 class ISerializable 
{
public:
	virtual void Serialize(std::ostream& stream) const {};
	virtual void SerializeJSON(std::ostream& stream) const {};
	virtual void Deserialize(std::istream& stream) {};
	virtual void DeserializeJSON(std::istream& stream) {};
	virtual void DeserializeJSON(UtilsJSON::Object& obj) {};

};

