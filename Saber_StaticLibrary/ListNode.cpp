#include "pch.h"
#include "ListNode.h"
#include "UtilsJSON.h"

ListNode::ListNode(const std::string& Data)
{
	this->Data = Data;
	Prev = nullptr;
	Next = nullptr;
	Rand = nullptr;
	f_GetKeyForNode = nullptr;
	f_GetNodeForKey = nullptr;
}

void ListNode::SerializeJSON(std::ostream& stream) const
{
	UtilsJSON::Object objJSON;
	objJSON.AddPointer("Data", &Data);

	if (f_GetKeyForNode != nullptr)
	{
		objJSON.AddValue("Prev", f_GetKeyForNode(Prev));
		objJSON.AddValue("Next", f_GetKeyForNode(Next));
		objJSON.AddValue("Rand", f_GetKeyForNode(Rand));
	}

	objJSON.WriteTo(stream);

}

void ListNode::DeserializeJSON(std::istream& stream)
{
	UtilsJSON::Object obj;
	if (obj.ReadFrom(stream))
		DeserializeJSON(obj);
}

void ListNode::DeserializeJSON(UtilsJSON::Object& obj)
{
	obj.GetValue("Data", Data);
	//if (f_GetNodeForKey != nullptr)
	{
		int index;
		if (obj.GetValue("Prev", index))
			Prev = f_GetNodeForKey(index);
		if (obj.GetValue("Next", index))
			Next = f_GetNodeForKey(index);
		if (obj.GetValue("Rand", index))
			Rand = f_GetNodeForKey(index);
	}
}
