#pragma once
#include <string>
#include <functional>
#include "ISerializable.h"

class ListNode : public ISerializable 
{
public:
    //Previous node in the List
    ListNode* Prev; 

    //Next Node in the list
    ListNode* Next;

    //Random node in the List
    ListNode* Rand; 

    //Node Data
    std::string Data;

    //Required for serialization 
    std::function<int(ListNode*)> f_GetKeyForNode;

    // Required for deserialization
    std::function<ListNode* (int)> f_GetNodeForKey;

    ListNode(const std::string& Data = "");

    //Begin Serialize Interface
    virtual void SerializeJSON(std::ostream& stream) const override;
    virtual void DeserializeJSON(std::istream& stream) override;
    virtual void DeserializeJSON(UtilsJSON::Object& obj) override;
    //End Serialize Interface
};

