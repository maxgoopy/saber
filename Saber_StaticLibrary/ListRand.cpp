#include "pch.h"
#include "ListRand.h"
#include "UtilsJSON.h"

void ListRand::RefreshListMap()
{
	if (ListMap != nullptr)
		delete ListMap;

	ListMap = new std::map<ListNode*, int>;

	ListNode* iter = Head;
	int i = -1;
	while (iter != nullptr)
	{
		ListMap->insert({ iter, ++i });
		iter = iter->Next;
	}

	ListMapValid = true;
}

void ListRand::CreateNodes(size_t number)
{
	if (number <= 0)
		return;
	for (int counter = 0; counter < number; ++counter)
		Append(new ListNode);
}

ListRand::~ListRand()
{
	ListNode* node = Head;
	ListNode* delNode = nullptr;
	while (node != nullptr)
	{
		delNode = node;
		node = node->Next;
		delete delNode;
	}
	if (ListMap != nullptr)
		delete ListMap;
}

ListNode* ListRand::Append(ListNode* Node)
{
	if (Head == nullptr)
	{
		Head = Node;
		Tail = Node;
		Count = 1;

	}
	else
	{
		Node->Prev = Tail;
		Tail->Next = Node;
		Tail = Node;
		++Count;
	}

	Node->f_GetKeyForNode = [this](ListNode* Node) { return GetNodeNumber(Node); };
	Node->f_GetNodeForKey = [this](size_t index) {return (*this)[index]; };
	ListMapValid = false;

	return Node;
}

void ListRand::Serialize(std::ostream& stream) const
{
	//Write Count
	stream << Count << " "; 

	//Write Nodes data
	ListNode* node = Head;
	int nodeNumber = 0;
	while (node != nullptr)
	{
		//Write node number
		stream << nodeNumber++ << " ";

		//Write data
		SerializeString(stream, node->Data);
		stream << " ";

		//Write node number for Rand 
		stream << node->f_GetKeyForNode(node->Rand)<< " ";

		node = node->Next;
	}
}

void ListRand::SerializeJSON(std::ostream& stream) const
{
	//Create JSON data object
	UtilsJSON::Object objJson;

	//Add Count by pointer
	objJson.AddPointer<int>("Count", &Count);

	//Add Serializeble nodes by pointer to JSON data object
	auto node = Head;
	while (node != nullptr)
	{
		objJson.AddArrayValuePointer<ISerializable>("Nodes", node);
		node = node->Next;
	}

	//Write JSON data in stream
	objJson.WriteTo(stream);
}

void ListRand::Deserialize(std::istream& stream)
{
	//Deserialize only for empty list
	if (Head != nullptr || Tail != nullptr)
	{
		std::cout << " Deserialization failed : List not empty.";
		return;
	}
	
	//Read Count
	int desCount;
	stream >> desCount;
	if (desCount > 0)
	{
		ListNode** arrayNodes = new ListNode * [desCount];

		//Init Nodes
		for (int i = 0; i < desCount; ++i)
			arrayNodes[i] = Append(new ListNode);

		std::string data;
		int number;
		int randNumber;

		//Set proper data and rand node
		for (int i = 0; i < desCount; ++i)
		{
			stream >> number;
			data.clear();
			DeserializeString(stream, data);
			stream >> randNumber;
			arrayNodes[i]->Data = data;
			if (randNumber != -1)
				arrayNodes[i]->Rand = arrayNodes[randNumber];
		}

		delete[] arrayNodes;
	}

}

void ListRand::DeserializeJSON(std::istream& stream)
{
	//Deserialize only for empty list
	if (Head != nullptr || Tail != nullptr)
	{
		std::cout << " Deserialization failed : List not empty.";
		return;
	}

	//Read JSON data from stream
	UtilsJSON::Object obj;
	if (!obj.ReadFrom(stream))
	{
		std::cout << "Deserialization failed";
		return;
	}

	//Get Count from JSON data
	int desCount;
	if (!obj.GetValue("Count", desCount))
		desCount = (int)obj.GetArrayLength("Nodes");

	if (desCount <= 0)
		return;

	//Create empty nodes
	ListNode* node;
	ListNode** arrayNodes = new ListNode * [desCount];
	auto f_GetNodeForKey = [&arrayNodes, &desCount](size_t index) -> ListNode*
	{
		if (index >= 0 && index < desCount)
			return arrayNodes[index];
		return nullptr;
	};
	for (int i = 0; i < desCount; ++i)
	{
		node = new ListNode;
		//Create by Append for setting proper f_GetKeyForNode function
		Append(node);
		arrayNodes[i] = node;
		//Set castom f_GetNodeForKey for more optimization
		node->f_GetNodeForKey = f_GetNodeForKey;
	}
	
	//Deserialize nodes with JSON object
	UtilsJSON::Object* nodeObj;
	for (int i = 0; i < Count; ++i)
	{
		if(obj.GetArrayValue("Nodes", i, nodeObj))
			arrayNodes[i]->DeserializeJSON(*nodeObj);
	}
	
	//Go throw created nodes
	for (int i = 1; i < Count; ++i)
	{
		//Set f_GetNodeForKey to default method
		arrayNodes[i]->f_GetNodeForKey = [this](size_t index) {return (*this)[index]; };

		//Set proper Head
		if (arrayNodes[i]->Prev == nullptr)
			Head = arrayNodes[i];

		//Set proper Tail
		if (arrayNodes[i]->Next == nullptr)
			Tail = arrayNodes[i];
	}

}

void ListRand::SerializeString(std::ostream& stream, const std::string& str) const
{
	stream << '\"';
	for (auto iter = str.begin(); iter != str.end(); ++iter)
	{
		if (specialChar.find(*iter) != std::string::npos)
			stream << '\\';
		stream << *iter;
	}
	stream << '\"';
}

void ListRand::DeserializeString(std::istream& stream, std::string& str)
{
	char ch = ' ';
	while (ch != '"' && !stream.eof())
		stream.get(ch);
	stream.get(ch);
	while (ch != '"' && !stream.eof())
	{
		if (ch == '\\')
			stream.get(ch);
		str += ch;
		stream.get(ch);
	}
}

int ListRand::GetNodeNumber(ListNode* node)
{
	if (node == nullptr)
		return -1;

	if (!ListMapValid)
		RefreshListMap();

	auto list = ListMap->find(node);
	
	//Pointer not to our ListNode
	if (list == ListMap->end())
		return -1;

	return list->second;
}

ListNode* ListRand::operator[](size_t index)
{
	if (index >= Count || index < 0)
		return nullptr;
	ListNode* result = Head;
	for (int i = 0; i < index; ++i)
		result = result->Next;
	return result;
}
