#pragma once
#include "ListNode.h"
#include "ISerializable.h"
#include <map>

class ListRand : public ISerializable
{
private:
	//ListNodes map. Required for serialization
	std::map<ListNode*, int>* ListMap = nullptr;
	//Flag if ListMap has proper value
	bool ListMapValid = false;

	//Special characters, which must be proper serialize in Simple Serialization
	std::string specialChar = "\\\"";

public:
	//Head Node
	ListNode* Head = nullptr;

	//Tail Node
	ListNode* Tail = nullptr;

	//Node Count
	int Count = 0;

private:
	//Update ListMap
	void RefreshListMap();

public:

	~ListRand();

	//Append new node to the tail of list
	ListNode* Append(ListNode* Node);

	//Create set of empty Nodes
	void CreateNodes(size_t number);

	//Begin Serialize Interface
	void Serialize(std::ostream& stream) const override;
	void SerializeJSON(std::ostream& stream) const override;
	void Deserialize(std::istream& stream) override;
	void DeserializeJSON(std::istream& stream) override;
	//End Serialize Interface

	//Serialize string to the strem
	void SerializeString(std::ostream& stream, const std::string& str) const;

	//Deserialize string from stream
	void DeserializeString(std::istream& stream, std::string& str);

	//Get node Number from ListMap
	int GetNodeNumber(ListNode* node);

	ListNode* operator[](size_t index);


};

