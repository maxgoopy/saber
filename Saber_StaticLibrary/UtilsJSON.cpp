#include "pch.h"
#include "UtilsJSON.h"

UtilsJSON::Object::~Object()
{
	for (auto iter : dataPObjJSON)
		delete iter.second;

	for (auto iter : dataPArrayObjJSON)
	{
		for (auto iter2 : iter.second->data)
			delete iter2;
		delete iter.second;
	}
}

bool UtilsJSON::Object::ReadVarName(std::istream& stream, std::string& outName)
{
	char separator = '"';
	std::string blackList = "{}[]:,";

	if (!MoveIStreamToSeparator(stream, separator, blackList))
		return false;
	stream.get();
	
	char ch;
	stream >> ch;
	while (ch != separator)
	{
		if (stream.eof())
			return false;

		outName += ch;
		ch = stream.get();
		
	}

	return true;
}

bool UtilsJSON::Object::ReadVarValueString(std::istream& stream, std::string& outValue)
{
	char separator = '"';
	char ch = ' ';
	if (!MoveIStreamToSeparator(stream, separator, "{}[]:,"))
		return false;
	stream.get();

	bool stringEnd = false;
	while (!stringEnd)
	{
		ch = stream.get();

		if (stream.eof())
			return false;

		if (ch == separator)
			stringEnd = true;
		else if (ch == '\\')
		{
			ch = stream.get();
			outValue += ch;

		}
		else
			outValue += ch;
	}

	return true;
}

bool UtilsJSON::Object::ReadVarValueFloat(std::istream& stream, float& outValue)
{
	stream >> outValue;
	return true;
}

bool UtilsJSON::Object::ReadVarValueObject(std::istream& stream, Object& outValue)
{
	return outValue.ReadFrom(stream);
}

bool UtilsJSON::Object::ReadArrayOfObjects(std::istream& stream, Array<Object*>& outValue)
{
	//Move to begin of Array
	if (!MoveIStreamToSeparator(stream, '[', "{}]\":,"))
		return false;
	stream.get();

	bool endOfArray = false;
	bool objectFound = false;
	bool requiredComma = false;

	//Parse array
	while (!endOfArray)
	{

		if (stream.eof())
			return false;
		//Found array end
		if (MoveIStreamToSeparator(stream, ']', "{}[\":,"))
			endOfArray = true;
		//Find comma if required
		else if (requiredComma)
			if (MoveIStreamToSeparator(stream, ',', "{}[]\":"))
			{
				requiredComma = false;
				stream.get();
			}
			else
				return false;
		//Found object begin
		else if (MoveIStreamToSeparator(stream, '{', "}[]\":,"))
			objectFound = true;
		//Parsing error
		else
			return false;

		//If begin object was found read them and add to outValue
		if (objectFound)
		{
			Object* obj = new Object;
			if (!obj->ReadFrom(stream))
			{
				//Something go wrong while parsing
				delete obj;
				return false;
			}

			outValue.Append(obj);
			objectFound = false;
			requiredComma = true;
		}
	}

	//Move stream on ']'
	stream.get();
	return true;
}

bool UtilsJSON::Object::ReadProperty(std::istream& stream)
{
	std::string propertyName;
	
	if (!ReadVarName(stream, propertyName))
		return false;
	
	if (!MoveIStreamToSeparator(stream, ':', "{}[]\","))
		return false;
	stream.get();

	//Find next not whitespace char

	while (std::isspace(stream.peek()))
	{
		stream.get();
		if (stream.eof())
			return false;
	}

	char ch = stream.peek();
	if (std::isdigit(ch) || ch =='-')
		ch = '0';

	switch (ch)
	{
	case '0':
	{
		float value;
		if (ReadVarValueFloat(stream, value))
			dataVInt[propertyName] =(int)value;
		else
			return false;
	}
		break;
	case '"':
	{
		std::string value;
		if (ReadVarValueString(stream, value))
			dataVStr[propertyName] = value;
		else
			return false;
	}
	break;
	case '[':
	{
		Array<Object*>* value = new Array<Object*>;
		if (ReadArrayOfObjects(stream, *value))
			dataPArrayObjJSON[propertyName] = value;
		else
		{
			delete value;
			return false;
		}
	}
	break;
	case '{':
	{
		Object* value = new Object;
		if (ReadVarValueObject(stream, *value))
			dataPObjJSON[propertyName] = value;
		else
		{
			delete value;
			return false;
		}
	}
	break;
	default:
		return false;
		break;
	}
	return true;
}

bool UtilsJSON::Object::MoveIStreamToSeparator(std::istream& stream, char separator, const std::string& blacklist) const 
{
	char ch = stream.peek();
	
	while (ch != separator)
	{
		if (blacklist.find(ch) != std::string::npos || stream.eof())
		{
			return false;
		}
		else
			stream.get();
		ch = stream.peek();
	}
	return true;
}

void UtilsJSON::Object::SerializeString(std::ostream& stream, const std::string& str) const 
{
	stream << '\"';
	for (auto iter = str.begin(); iter != str.end(); ++iter)
	{
		if (specialChar.find(*iter) != std::string::npos)
			stream << '\\';
		stream << *iter;
	}
	stream << '\"';
}

size_t UtilsJSON::Object::GetArrayLength(const std::string& Name)
{
	auto iter = dataPArrayObjJSON.find(Name);
	if (iter == dataPArrayObjJSON.end())
		return 0;
	return iter->second->data.size();
}

void UtilsJSON::Object::WriteTo(std::ostream& stream) const
{
	bool commaRequired = false;
	stream << '{' << std::endl;
	for (auto node : dataPInt)
	{
		if (commaRequired)
			stream << ',' << std::endl;
		stream << '"' << node.first << "\": " << *node.second;
		commaRequired = true;
	}

	for (auto node : dataVInt)
	{
		if (commaRequired)
			stream << ',' << std::endl;
		stream << '"' << node.first << "\": " << node.second;
		commaRequired = true;
	}

	for (auto node : dataPStr)
	{
		if (commaRequired)
			stream << ',' << std::endl;
		stream << '"' << node.first << "\": ";
		SerializeString(stream, *(node.second));
		commaRequired = true;
	}

	for (auto node : dataPObj)
	{
		if (commaRequired)
			stream << ',' << std::endl;
		stream << '"' << node.first << "\": " << std::endl;
		node.second->SerializeJSON(stream);
		commaRequired = true;
	}

	for (auto node : dataPArrayObj)
	{
		if (commaRequired)
			stream << ',' << std::endl;
		stream << '"' << node.first << "\": " << std::endl;
		node.second->WriteTo(stream);
		commaRequired = true;
	}

	stream << std::endl << '}';
}

bool UtilsJSON::Object::ReadFrom(std::istream& stream)
{
	if (stream.eof())
		return false;

	//Move to begin of object
	if(!MoveIStreamToSeparator(stream,'{', "}[]\":,"))
		return false;
	//Get '{'
	stream.get();
	
	bool endOfObject = false;
	bool propertyFound = false;
	bool requiredComma = false;

	while (!endOfObject)
	{
		if (MoveIStreamToSeparator(stream, '}', "{[]\":,"))
			endOfObject = true;
		else if (requiredComma)
			if (MoveIStreamToSeparator(stream, ',', "{}[]\":"))
			{
				stream.get();
				requiredComma = false;
			}
			else
				return false;
		else if (MoveIStreamToSeparator(stream, '"', "{}[]:,"))
			propertyFound = true;
		else
			return false;

		if (propertyFound)
		{
			if (!ReadProperty(stream))
				return false;
			propertyFound = false;
			requiredComma = true;
		}
	
	}
	//Get '}'
	stream.get();

	return true;
}

//void UtilsJSON::Array::WriteTo(std::ostream& stream)
//{
//	stream << '[' << std::endl;
//	std::list<const ISerializable*>::iterator iter = List.begin();
//	if (iter!=List.end())
//		(*iter)->SerializeJSON(stream);
//
//	++iter;
//
//	while (iter != List.end())
//	{
//		stream << ',' << std::endl;
//		(*(iter++))->SerializeJSON(stream);
//	}
//	stream << std::endl << ']';
//}



