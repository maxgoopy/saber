#pragma once
#include <string>
#include <map>
#include <iostream>
#include <vector>
#include "ISerializable.h"

namespace UtilsJSON
{
	//Custom array with WriteTo stream function
	template<typename T>
	class Array {
	public:
		std::vector<T> data;

		void WriteTo(std::ostream& stream);

		void Append( T Obj);
	};

	//JSON object whith methods for Serialization/Deserialization data from stream
	class Object {

		//Special characters, which must be proper serialize in Simple Serialization
		std::string specialChar = "\\\"";

		//Maps for contain data for serialization by Pointer or by Value
		std::map < std::string, const int* > dataPInt;
		std::map < std::string, int > dataVInt;
		std::map < std::string, const std::string* > dataPStr;
		std::map < std::string, std::string > dataVStr;
		std::map < std::string, const ISerializable* > dataPObj;
		std::map < std::string, Object* > dataPObjJSON;
		std::map < std::string, Array<const ISerializable*>* > dataPArrayObj;
		std::map < std::string, Array<Object*>* > dataPArrayObjJSON;

	private:
		//Deserialization methods
		//Read variable name
		bool ReadVarName(std::istream& stream, std::string& outName);

		//Read string variable value
		bool ReadVarValueString(std::istream& stream, std::string& outValue);

		//Read float variable value
		bool ReadVarValueFloat(std::istream& stream, float& outValue);

		//Read variable value as JSON Object
		bool ReadVarValueObject(std::istream& stream, Object& outValue);

		//Read array of objects variable
		bool ReadArrayOfObjects(std::istream& stream, Array<Object*>& outValue);

		//Read property from JSON and save it proper
		bool ReadProperty(std::istream& stream);

		//Move stream pointer before next separator or value from blacklist
		//@return true if reach separator, in other case (reach blacklist value or end of stream) return false
		bool MoveIStreamToSeparator(std::istream& stream, char separator, const std::string& blacklist = "") const;

		//Proper serialize string value in stream
		void SerializeString(std::ostream& stream, const std::string& str) const;

	public:
		~Object();

		//Add variable for serialization by Pointer
		template<typename T>
		void AddPointer(const std::string& varName, const T* data);

		//Add variable for serialization by value
		template<typename T>
		void AddValue(const std::string& varName, const T data);

		//Add Value by pointer in to Array whith arrayName
		template<typename T>
		void AddArrayValuePointer(const std::string& arrayName, const T* arrayValue);

		//Get Value from variable whith name Name
		//@return true if that variable exist and outValue valid
		template<typename T>
		bool GetValue(const std::string& Name, T& outValue);

		//Get Value from array whith name Name at position index
		//@return true if that variable exist and outValue valid
		template<typename T>
		bool GetArrayValue(const std::string& Name, const int& index, T& outValue);

		//Get size of Array whith name Name
		//If Array dont exist return 0
		size_t GetArrayLength(const std::string& Name);

		//Write JSON data to stream
		void WriteTo(std::ostream& stream) const;

		//Read JSON data from stream
		bool ReadFrom(std::istream& stream);

	};
	
	template<>
	inline void Object::AddPointer<int>(const std::string& varName, const int* data)
	{
		dataPInt[varName] = data;
	}

	template<>
	inline void Object::AddValue<int>(const std::string& varName, const int data)
	{
		dataVInt[varName] = data;
	}

	template<>
	inline void Object::AddPointer<std::string>(const std::string& varName, const std::string* data)
	{
		dataPStr[varName] = data;
	}
	
	template<>
	inline void Object::AddPointer<ISerializable>(const std::string& varName, const ISerializable* data)
	{
		dataPObj[varName] = data;
	}

	template<>
	inline void Object::AddArrayValuePointer<ISerializable>(const std::string& arrayName, const ISerializable* arrayValue)
	{
		auto item = &dataPArrayObj[arrayName];
		if (*item == nullptr)
			*item = new Array<const ISerializable*>;
		(*item)->Append(arrayValue);
	}


	inline void UtilsJSON::Array<const ISerializable*>::WriteTo(std::ostream& stream)
	{
		stream << '[' << std::endl;
		auto iter = data.begin();
		if (iter != data.end())
			(*iter)->SerializeJSON(stream);

		++iter;

		while (iter != data.end())
		{
			stream << ',' << std::endl;
			(*(iter++))->SerializeJSON(stream);
		}
		stream << std::endl << ']';
	}

	inline void UtilsJSON::Array<Object*>::WriteTo(std::ostream& stream)
	{
		stream << '[' << std::endl;
		auto iter = data.begin();
		if (iter != data.end())
			(*iter)->WriteTo(stream);

		++iter;

		while (iter != data.end())
		{
			stream << ',' << std::endl;
			(*(iter++))->WriteTo(stream);
		}
		stream << std::endl << ']';
	}

	template<typename T>
	inline void Array<T>::Append( T Obj)
	{
		data.push_back(Obj);
	}

	template<>
	inline bool Object::GetValue<int>(const std::string& Name, int& outValue)
	{
		auto iter = dataVInt.find(Name);
		if(iter == dataVInt.end())
			return false;

		outValue = iter->second;
		return true;
	}

	template<>
	inline bool Object::GetValue<std::string>(const std::string& Name, std::string& outValue)
	{
		auto iter = dataVStr.find(Name);
		if (iter == dataVStr.end())
			return false;

		outValue = iter->second;
		return true;
	}

	template<>
	inline bool Object::GetArrayValue<Object>(const std::string& arrayName, const int& index, Object& outValue)
	{
		auto item = dataPArrayObjJSON.find(arrayName);
		if (item == dataPArrayObjJSON.end())
			return false;

		auto arr = item->second;
		if (arr->data.size() <= index)
			return false;

		outValue = *(arr->data[index]);
		return true;
	}

	template<>
	inline bool Object::GetArrayValue<Object*>(const std::string& arrayName, const int& index, Object*& outValue)
	{
		auto item = dataPArrayObjJSON.find(arrayName);
		if (item == dataPArrayObjJSON.end())
			return false;

		auto arr = item->second;
		if (arr->data.size() <= index)
			return false;

		outValue = arr->data[index];
		return true;
	}

	

};
