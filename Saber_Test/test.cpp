#include "pch.h"
#include <string>

class RandListFixture :public::testing::Test
{
public:
	ListRand *Original;
	ListRand *Copy;
	
	void SetUp() override
	{
		Original = new ListRand;
		Original->CreateNodes(11);
		for (int i = 0; i < 11; ++i)
			(*Original)[i]->Data = std::to_string(i);

		(*Original)[1]->Data = "one";
		(*Original)[2]->Data = "Two Second";
		(*Original)[3]->Data = "3_";
		(*Original)[4]->Data = "4\\\"";
		(*Original)[5]->Data = "5\\\\";
		(*Original)[6]->Data = "6::";
		(*Original)[7]->Data = "7}{";
		(*Original)[8]->Data = "8][";
		(*Original)[9]->Data = "9{}[]";
		(*Original)[9]->Data = "10\n\t\r";

		(*Original)[0]->Rand = (*Original)[9];
		(*Original)[1]->Rand = (*Original)[3];
		(*Original)[2]->Rand = (*Original)[0];
		(*Original)[5]->Rand = (*Original)[5];
		(*Original)[9]->Rand = (*Original)[0];
		(*Original)[9]->Rand = (*Original)[2];

		Copy = new ListRand;
	}

	void TearDown() override
	{
		delete Original;
		delete Copy;
	}

};

TEST_F(RandListFixture, Serialization_Deserialization)
{
	std::stringstream stream;
	Original->Serialize(stream);

	Copy->Deserialize(stream);

	EXPECT_EQ(Copy->Count, Original->Count);
	for (int i = 0; i < Original->Count; ++i)
	{
		EXPECT_EQ((*Original)[i]->Data, (*Copy)[i]->Data);
		if ((*Original)[i]->Rand == nullptr)
			EXPECT_EQ((*Copy)[i]->Rand, nullptr);
		else
			EXPECT_EQ((*Original)[i]->Rand->Data, (*Copy)[i]->Rand->Data);
	}

	
}

TEST_F(RandListFixture, Serialization_Deserialization_JSON)
{
	std::stringstream stream;
	Original->SerializeJSON(stream);

	Copy->DeserializeJSON(stream);

	EXPECT_EQ(Copy->Count, Original->Count);
	for (int i = 0; i < Original->Count; ++i)
	{
		EXPECT_EQ((*Original)[i]->Data, (*Copy)[i]->Data);
		if ((*Original)[i]->Rand == nullptr)
			EXPECT_EQ((*Copy)[i]->Rand, nullptr);
		else
			EXPECT_EQ( (*Copy)[i]->Rand->Data, (*Original)[i]->Rand->Data);
	}


}
